# The Sudoku Project

The goal of this project was to investigate a variety of algorithms (backtracking, brute force, stochastic search and Crook’s algorithm) that are capable of solving sudoku puzzles, of ranging difficulties, in order to learn more about sudoku solving techniques.

We also wanted to use the OpenCV library to read a sudoku from an image and solve it. This was a project to feed our curiosity in the domains of computer vision and deep learning.




## Authors

- [@yashvidonga](https://www.gitlab.com/yashvidonga29)
- [@ishikade](https://www.gitlab.com/dmishika2002)



## Tech Stack

**Languages:** C++, Java, Python, Haskell and Elixir

**Python Libraries:** OpenCV, Keras, Numpy


## Description and Results

- We tested each algorithm in C++, Java and Python for 100 different randomly generated puzzles. The table below shows the results of the same.
![Average time taken to solve 100 sudokus](./project_progress/weekly_presentation/week9_img/data.png)
- Below is a screenshot of how image looks after image processing and how the sudoku was read and solved from the image.
![Sudoku solved](./project_progress/weekly_presentation/week9_img/transformation.png)
![Sudoku solved](./project_progress/weekly_presentation/week9_img/solve.png)

- We also implemented Peter Norvig's Sudoku Solver in Elixir and backtracking algorithm in Haskell.

## Future Scope

- Explore more algorithms like dancing links algorithm


## Feedback

If you have any feedback, please reach out to us at dmishika2002@gmail.com or yashvidonga29@gmail.com .

