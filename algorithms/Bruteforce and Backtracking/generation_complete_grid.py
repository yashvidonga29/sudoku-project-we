import math
from random import shuffle, randint

empty_grid = [[0 for i in range(9)] for j in range(9)]
num_list = [1, 2, 3, 4, 5, 6, 7, 8, 9]

def print_grid(grid: [[int]]):
    for row in grid:
        print(row)

def generate_sudoku(grid: [[int]]):  
    length = len(grid)
    for row in range(length):
        for col in range(length):
            if grid[row][col] == 0:
                shuffle(num_list)
                for value in num_list:
                    if (isValidCell(grid, row, col, value)):
                        grid[row][col] = value
                        if generate_sudoku(grid):
                            return True
                        grid[row][col] = 0
                return False
    return True    

def isValidCell(grid: [[int]], row: int, col: int, num: int):
    return (not (present_in_Row(grid, row, num)) and not (present_in_Col(grid, col, num)) and not (present_in_Box(grid, row, col, num)))

def present_in_Row(grid: [[int]], row: int, num: int):
    for col in range(len(grid)):
        if num == grid[row][col]:
            return True
    return False

def present_in_Col(grid: [[int]], col: int, num: int):
    for row in range(len(grid)):
        if num == grid[row][col]:
            return True
    return False

def present_in_Box(grid: [[int]], row: int, col: [int], num: int):
    box_size = int(math.sqrt(len(grid)))
    sub_row = (row // box_size) * box_size
    sub_col = (col // box_size) * box_size
    for i in range(box_size):
        for j in range(box_size):
            if num == grid[sub_row + i][sub_col + j]:
                return True
    return False

def solve_sudoku(grid):  # grid
    length = len(grid)
    for row in range(length):
        for col in range(length):
            if grid[row][col] == 0:
                for num in range(1, length + 1):
                    if (isValidCell(grid, row, col, num)):
                        grid[row][col] = num
                        if solve_sudoku(grid):
                            return True
                        grid[row][col] = 0
                return False

generate_sudoku(empty_grid)
print_grid(empty_grid)