#include <bits/stdc++.h>
using namespace std;

const int grid_size = 9;

int empty_grid[9][9] = {{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
						{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
						{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
						{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
						{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
						{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
						{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
						{ 0, 0, 0, 0, 0, 0, 0, 0, 0 },
						{ 0, 0, 0, 0, 0, 0, 0, 0, 0 }};

int num_list[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};

void print_grid(int arr[grid_size][grid_size]) {
    for (int row = 0; row < grid_size; row++) {
        for (int column = 0; column < grid_size; column++) {
            cout << arr[row][column] << " ";
        }
        cout << endl;
    }
}

bool present_in_Row(int grid[grid_size][grid_size], int row, int num) {
    for (int col = 0; col < grid_size; col++) {
        if (num == grid[row][col]) {
            return true;
        }
    }
    return false;
}

bool present_in_Col(int grid[grid_size][grid_size], int col, int num) {
    for (int row = 0; row < grid_size; row++) {
        if (num == grid[row][col]) {
            return true;
        }
    }
    return false;
}

bool present_in_Box(int grid[grid_size][grid_size], int row, int col, int num) {
    int box_size = int(sqrt(grid_size));
    int sub_row = (row / box_size) * box_size;
    int sub_col = (col / box_size) * box_size;
    for (int i = 0; i < box_size; i++) {
        for (int j = 0; j < box_size; j++) {
            if (num == grid[sub_row + i][sub_col + j]) {
                return true;
            }
        }
    }
    return false;
}

bool isValidCell(int grid[grid_size][grid_size], int row, int col, int num) {
    return (!(present_in_Row(grid, row, num)) && !(present_in_Col(grid, col, num)) && !(present_in_Box(grid, row, col, num)));
}

bool generate_sudoku(int grid[grid_size][grid_size], int num_list[grid_size]) {
    for (int row = 0; row < grid_size; row++) {
        for (int col = 0; col < grid_size; col++) {
            if (grid[row][col] == 0) {
                srand (time(NULL));
                random_shuffle(num_list, num_list + grid_size);
                for (int value = 0; value < grid_size; value++) {
                    if (isValidCell(grid, row, col, num_list[value])) {
                        grid[row][col] =  num_list[value];
                        if (generate_sudoku(grid, num_list)) {
                            return true;
                        }
                        grid[row][col] = 0;
                    }
                }
                return false;
            }
        }
    }
    return true;
}

int main() {
    generate_sudoku(empty_grid, num_list);
    print_grid(empty_grid);
    return 0;
}