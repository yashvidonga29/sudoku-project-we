#include <bits/stdc++.h>
using namespace std;

const int grid_size = 9;
int puzzle[9][9] = {    { 8, 0, 0, 0, 0, 0, 0, 0, 0 },
						{ 0, 0, 3, 6, 0, 0, 0, 0, 0 },
						{ 0, 7, 0, 0, 9, 0, 2, 0, 0 },
						{ 0, 5, 0, 0, 0, 7, 0, 0, 0 },
						{ 0, 0, 0, 0, 4, 5, 7, 0, 0 },
						{ 0, 0, 0, 1, 0, 0, 0, 3, 0 },
						{ 0, 0, 1, 0, 0, 0, 0, 6, 8 },
						{ 0, 0, 8, 5, 0, 0, 0, 1, 0 },
						{ 0, 9, 0, 0, 0, 0, 4, 0, 0 }};

void print_grid(int arr[grid_size][grid_size]) {
    for (int row = 0; row < grid_size; row++) {
        for (int col = 0; col < grid_size; col++) {
            cout << arr[row][col] << " ";
        }
        cout << endl;
    }
}

bool present_in_Row(int grid[grid_size][grid_size], int row, int num) {
    for (int col = 0; col < grid_size; col++) {
        if (num == grid[row][col]) {
            return true;
        }
    }
    return false;
}

bool present_in_Col(int grid[grid_size][grid_size], int col, int num) {
    for (int row = 0; row < grid_size; row++) {
        if (num == grid[row][col]) {
            return true;
        }
    }
    return false;
}

bool present_in_Box(int grid[grid_size][grid_size], int row, int col, int num) {
    int box_size = int(sqrt(grid_size));
    int sub_row = (row/box_size) * box_size;
    int sub_col = (col/box_size) * box_size;
    for (int i = 0; i < box_size; i++) {
        for (int j = 0; j < box_size; j++) {
            if (num == grid[sub_row + i][sub_col + j]) {
                return true;
            }
        }
    }
    return false;
}

bool isValidCell(int grid[grid_size][grid_size], int row, int col, int num) {
    return (!(present_in_Row(grid, row, num)) && !(present_in_Col(grid, col, num)) && !(present_in_Box(grid, row, col, num)));
}

bool solve_sudoku(int grid[grid_size][grid_size]) {
    for (int row = 0; row < grid_size; row++) {
        for (int col = 0; col < grid_size; col++) {
            if (grid[row][col] == 0) {
                for (int num = 1; num < grid_size + 1; num++) {
                    if (isValidCell(grid, row, col, num)) {
                        grid[row][col] = num;
                        if (solve_sudoku(grid)){
                            return true;
                        }
                        grid[row][col] = 0;
                    }
                }
                return false;
            }
        }
    }
    return true;
}

int main() {
    solve_sudoku(puzzle);
    print_grid(puzzle);
    return 0;
}