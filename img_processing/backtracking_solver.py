import math
import time
from generating_grid import sudoku_puzzle

def print_grid(grid: [[int]]):
    for num in grid:
        print(num)

def solve_sudoku(grid: [[int]]):  # grid
    length = len(grid)
    for row in range(length):
        for col in range(length):
            if grid[row][col] == 0:
                for num in range(1, length + 1):
                    if (isValidCell(grid, row, col, num)):
                        grid[row][col] = num
                        if solve_sudoku(grid):
                            return True
                        grid[row][col] = 0
                return False
    print_grid(grid)

def isValidCell(grid: [[int]], row: int, col: int, num: int):
    return (not (present_in_Row(grid, row, num)) and not (present_in_Col(grid, col, num)) and not (present_in_Box(grid, row, col, num)))

def present_in_Row(grid: [[int]], row: int, num: int):
    for col in range(len(grid)):
        if num == grid[row][col]:
            return True
    return False

def present_in_Col(grid: [[int]], col: int, num: int):
    for row in range(len(grid)):
        if num == grid[row][col]:
            return True
    return False

def present_in_Box(grid: [[int]], row: int, col: int, num: int):
    box_size = int(math.sqrt(len(grid)))
    sub_row = (row // box_size) * box_size
    sub_col = (col // box_size) * box_size
    for i in range(box_size):
        for j in range(box_size):
            if num == grid[sub_row + i][sub_col + j]:
                return True
    return False

def main():
    print("Image has been read.")
    print("Sudoku puzzle:")
    print_grid(sudoku_puzzle)
    t1 = time.time()
    print()
    print("Solved sudoku:")
    solve_sudoku(sudoku_puzzle)
    print()
    t2 = time.time()
    print()
    print("Time taken: ", t2 - t1, "seconds")
main()
