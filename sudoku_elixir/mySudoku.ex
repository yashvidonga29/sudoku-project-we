#Defining Helper functions
defmodule Helpers do
    def cross(rows, cols) do
        for a <- rows, b <- cols, do: [a, b]
    end
    def merge([l1, l2, l3]), do: l1 ++ l2 ++ l3
end

#########################################################################################################################

defmodule Sudoku do

    #Defining the required terminologies/Domain
    import Helpers
   
    @digits '123456789'
    @rows   'ABCDEFGHI'
    @cols   @digits
    @squares cross(@rows, @cols)  # A1, A2, .. I9
    @unitlist (for c <- @cols, do: cross(@rows, [c])) ++
              (for r <- @rows, do: cross([r], @cols)) ++
              (for rs <- ['ABC', 'DEF', 'GHI'], cs <- ['123', '456', '789'], do: cross(rs, cs))
    @units (for s <- @squares, into: %{} do
             {s, (for u <- @unitlist, s in u, do: u) }
           end)
    @peers (for s <- @squares, into: %{} do
            {s, @units[s] |> merge |> Enum.uniq |> Enum.reject(fn value -> value == s end) |> Enum.sort}
            end)
    
    #########################################################################################################################
    
    #parsing the grid
    def parse_grid(gridString) do
        values = for s <- @squares, into: %{}, do: {s, @digits}
        grid = grid_values(gridString)
        assign_into(grid, values) |> display
    end
    
    def grid_values(gridString) do
        Enum.zip(@squares, (for square <- gridString, do: [square]))
    end

    ###ASSIGN###
    def assign_into(grid_dict, values) do
        Enum.reduce(grid_dict, {:ok, values}, &assign_into_square/2)
    end

    def assign_into_square({s, [d]}, {:ok, values}) when d in @digits do
        assign(values, s, [d])
    end

    def assign_into_square({_s, [_d]}, values = {:ok, _}), do: values
    
    def assign_into_square(_, error), do: error                                                                    
   
    def assign(grid, s, d) do
        all_but_d = grid[s] -- d
        Enum.reduce(all_but_d, {{:ok, grid}, s}, &eliminate_d/2) |> elem(0)
    end

    ###ELIMINATION###
    def already_eliminated(values, s, digit) when is_integer(digit) and is_list(s) do
        cond do
            digit in values[s] -> nil
            true               -> values
        end
    end

    def eliminate_d(d, {{:ok, grid}, s}) when is_integer(d) do
        {eliminate(grid, s, d), s}
    end
  
    def eliminate_d(_d, other = {{:conflict, _message}, _s}), do: other
    
    def eliminate_s(s, {{:ok, values}, d}) do
        {eliminate(values, s, d), d}
    end

    def eliminate_s(_s, contradict_case = {{:contradiction, _message}, _d}) do
        contradict_case
    end

    def eliminate(values = %{}, s, digit) when is_integer(digit) do
        if already_eliminated(values, s, digit) do
            {:ok, values}
        else
          values
          |> update_in([s], &List.delete(&1, digit))
          |> update_square(s, values[s])
          |> update_related_units(s, digit)
        end
    end

    def update_square(_, s, []) do #if len(values[s]) == 0, removed last value
        {:contradiction, "Removed last value, no choices for #{s}"}
    end

    def update_square(grid, s, [d]) do #1 value only
        Enum.reduce(@peers[s], {{:ok, grid}, d}, &eliminate_s/2) |> elem(0)
    end

    def update_square(grid, s, value_ds) do #else nothing to do
        {:ok, grid}
    end

    def update_related_units(other = {:conflict, _message}, _s, _d), do: other
    
    def update_related_units({:ok, grid}, s, d) when is_list(s) and is_integer(d) do
        units = @units[s]
        Enum.reduce(units, {{:ok, grid}, s, d}, &update_a_unit/2) |> elem(0)
    end

    def update_a_unit(u, {{:ok, grid}, s, d})  when is_list(s) and is_integer(d) do
        dplaces = (for s <- u, d in grid[s], do: s)
        case length(dplaces) do
            0 -> { {:contradiction, "No options available, updating unit #{inspect u}"}, s, d}
            1 -> { assign(grid, hd(dplaces), [d]), s, d }
            _ -> { {:ok, grid}, s, d }
        end
    end
  
    def update_a_unit(_u, {other = {:contradiction, _message}, s, d}) do
        {other, s, d}
    end

    ### DISPLAYING THE GRID ###
    def display({message, grid}) do
        for r <- @rows do
        (for c <- @cols, do: grid[[r,c]])
        |> Enum.map(&square_to_string/1)
        |> Enum.chunk(3)
        |> Enum.intersperse('|')
        |> List.flatten
        |> IO.puts
        if r in 'CF' do
            IO.puts "---------+---------+---------"
        end
        end
        :ok
    end

    def square_to_string([d]), do: [?\s, d, ?\s]
    def square_to_string(_),   do: ' . '

    

end 

#IO.inspect Sudoku.grid_values("003020600900305001001806400008102900700000008006708200002609500800203009005010300")

