import Data.Char (isDigit)
import Data.List (delete, intersperse)

printGrid :: [Char] -> [Char]
printGrid [] = []
printGrid x = intersperse ' ' (take 9 x) ++ "\n" ++ printGrid (drop 9 x)

solve :: [Char] -> [Char]
solve x =
  if isSolved x
    then x
    else firstSolved cands
  where
    firstSolved [] = []
    firstSolved (c : cs) = if isSolved trial then trial else firstSolved cs
      where
        trial = solve c
    cands = possibleDigits x

isSolved :: [Char] -> Bool
isSolved x = (length x == 81) && notElem '0' x && isValidDigit x

possibleDigits :: [Char] -> [[Char]]
possibleDigits x = filter isValidDigit [takeWhile (/= '0') x ++ z ++ tail (dropWhile (/= '0') x) | z <- map show [1 .. 9]]

isValidDigit :: [Char] -> Bool
isValidDigit x = not $ any (isInvalidDigit x) [0 .. 80]

isInvalidDigit :: [Char] -> Int -> Bool
isInvalidDigit x i =
  (x !! i /= '0')
    && any (elem digit) [row, col, unit]
  where
    curr_row = mod i 9
    curr_col = quot i 9
    digit = x !! i
    row = [x !! a | a <- [9 * curr_col .. (9 * curr_col + 8)], a /= i]
    col = [x !! a | a <- [curr_row, (curr_row + 9) .. (curr_row + 72)], a /= i]
    sub_row = quot curr_row 3 * 3
    sub_col = quot curr_col 3 * 3
    si = (9 * sub_col) + sub_row
    unit = map (x !!) (delete i [si, si + 1, si + 2, si + 9, si + 10, si + 11, si + 18, si + 19, si + 20])

solveSudoku :: [Char] -> [Char]
solveSudoku = printGrid . solve

-- Exprected Input: "003020600900305001001806400008102900700000008006708200002609500800203009005010300"
-- Output: "4 8 3 9 2 1 6 5 7\n9 6 7 3 4 5 8 2 1\n2 5 1 8 7 6 4 9 3\n5 4 8 1 3 2 9 7 6\n7 2 9 5 6 4 1 3 8\n1 3 6 7 9 8 2 4 5\n3 7 2 6 8 9 5 1 4\n8 1 4 2 5 3 7 6 9\n6 9 5 4 1 7 3 8 2\n"